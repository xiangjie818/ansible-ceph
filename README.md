#### 安装准备
##### 1) 修改/etc/hosts文件
```
# Cluster Network
1.0.1.11 node1.cluster.com node1
1.0.1.12 node2.cluster.com node2
1.0.1.13 node3.cluster.com node3
1.0.1.14 node4.cluster.com node4
1.0.1.15 node5.cluster.com node5
1.0.1.16 node6.cluster.com node6
# Public Network
1.0.2.11 node1.public.com
1.0.2.12 node2.public.com
1.0.2.13 node3.public.com
1.0.2.14 node4.public.com
1.0.2.15 node5.public.com
1.0.2.16 node6.public.com
```

##### 2) 修改hosts文件
```
[mons]
1.0.1.11
1.0.1.12
1.0.1.13

[osds]
1.0.1.11
1.0.1.12
1.0.1.13

[mgrs]
1.0.1.11
1.0.1.12
1.0.1.13

[rgws]
1.0.1.11
1.0.1.12
1.0.1.13
```

##### 3) 根据集群状况修改group_vars/all.yml中的变量
```
ceph_version: nautilus                               # 安装的ceph版本
mirrors: 1.0.1.1                                     # 使用的yum源，如果使用外网源，可以设置为mirrors.aliyun.com
mon_initial_members: node1,node2,node3               # 定义mon节点
mon_host: 1.0.1.11,1.0.1.12,1.0.1.13                 # mon节点的IP
cluster_network: 1.0.1.0/24                          # 定义集群网络
public_network: 1.0.2.0/24                           # 定义外部网络
cluster_interface: eth0                              # 定义集群网络使用的网卡
chrony_mode: local                                   # 定义时钟同步方式，local为内网同步，外网同步使用internal
devices:                                             # 定义用来创建osd的磁盘
  - '/dev/sdb'
  - '/dev/sdc'
  - '/dev/sdd'
  - '/dev/sde'
```

#### 开始安装
```
ansible-playbook site.yml -i hosts
```
